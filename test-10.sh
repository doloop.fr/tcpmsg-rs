#!/bin/sh

_pids=""
date
mkdir -p log
for j in $(seq 1 20); do
    for i in $(seq 0 9); do
        ./target/release/examples/test_client "127.0.0.1:8723" 1>"log/logc-$j-$i" 2>"log/errc-$j-$i" &
        _pids="${_pids} $!"
    done
    sleep .125
done
wait ${_pids}
date
