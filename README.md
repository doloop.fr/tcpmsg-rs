# tcpmsg-rs
## Informations
This package/crate/library is an attempt to create a Rust client version of a library called `libtcp` from another project written in C. The main goal is to test the client in Rust, to replicate what the C test client is doing so that it can communicate with the C test server. A server side has been implemented in Rust, and everything is working as it's intended to.

If one were to use this code, it is the sol responsibility of the developer to use adequate configuration values with this Rust implementation and/or the C library. (no web link specified to the library for privacy purpose)
