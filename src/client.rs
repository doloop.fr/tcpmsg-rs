use std::{
    error::Error,
    io,
    net::SocketAddr,
    ops::Sub,
    time::{Duration, Instant},
};

use mio::{Events, Interest, Poll};

use crate::{
    config::{Config, StreamConfig},
    message::Message,
    msgtype::*,
    stream::Stream,
};

const CLIENT_TOKEN: mio::Token = mio::Token(0);
const CLIENT_TIMEOUT_CREATE: Duration = Duration::from_secs(5);
const CLIENT_TIMEOUT_RECV: Duration = Duration::from_micros(500);
const CLIENT_TIMEOUT_SEND: Duration = Duration::from_micros(500);

pub struct Client<'a> {
    inner: Poll,
    events: Events,
    msgtypes: Vec<MsgType<'a>>,
    stream: Stream,

    config: Config,

    last_event: Instant,
}

impl<'a> Client<'a> {
    pub fn new(addr: SocketAddr) -> io::Result<Client<'a>> {
        Client::new_with_config(addr, Config::default(), Some(CLIENT_TIMEOUT_CREATE))
    }

    pub fn new_with_timeout(addr: SocketAddr, timeout: Duration) -> io::Result<Client<'a>> {
        Client::new_with_config(addr, Config::default(), Some(timeout))
    }

    pub fn new_with_config(
        addr: SocketAddr,
        config: Config,
        timeout: Option<Duration>,
    ) -> io::Result<Client<'a>> {
        let timeout = timeout.map_or(CLIENT_TIMEOUT_CREATE, |t| t);
        // client is using non blocking mode only for connect
        let stdstream = std::net::TcpStream::connect_timeout(&addr, timeout)?;
        // let's just make sure we are in non blocking mode once connected
        stdstream.set_nonblocking(false)?;
        stdstream.set_read_timeout(Some(CLIENT_TIMEOUT_RECV))?;
        stdstream.set_write_timeout(Some(CLIENT_TIMEOUT_SEND))?;
        let miostream = mio::net::TcpStream::from_std(stdstream);
        miostream.set_nodelay(false)?;

        let poll = Poll::new()?;
        let mut stream = Stream::new(Some(miostream), Some(addr), Some(CLIENT_TOKEN));
        // register our stream
        poll.registry()
            .register(&mut stream, CLIENT_TOKEN, Interest::READABLE)?;

        let mut client = Self {
            stream,
            inner: poll,
            events: Events::with_capacity(1),
            msgtypes: Vec::new(),
            config,
            last_event: Instant::now().checked_sub(Duration::from_secs(1)).unwrap(),
        };
        client.stream.set_busy();

        Ok(client)
    }

    pub fn get_message(&mut self) -> Result<Option<Message>, Box<dyn std::error::Error>> {
        match self.stream.get_message(&self.msgtypes, &self.config)? {
            (Some(msg), false) => return Ok(Some(msg)),
            (None, true) => log::debug!("Closing stream"),
            _ => {}
        }

        if Instant::now().sub(self.last_event) > self.config.event_frequence() {
            self.last_event = Instant::now();

            self.inner
                .poll(&mut self.events, Some(Duration::from_millis(1)))?;
            for event in self.events.iter() {
                if !event.is_readable() {
                    continue;
                }
                if event.token() == CLIENT_TOKEN {
                    self.stream.receive(&self.config)?;
                    // let's register for more read since we do not loop until wouldblock
                    self.inner.registry().reregister(
                        &mut self.stream,
                        event.token(),
                        Interest::READABLE,
                    )?;
                }
            }
        }
        Ok(None)
    }

    pub fn queue_message_by_value(
        &mut self,
        value: i32,
        message: &[u8],
    ) -> Result<(), Box<dyn Error>> {
        let Some(msgtype) = self.msgtypes.iter().find(|m| m.value == value) else {
            return Err(format!("could not find message type by value [{}]", value).into());
        };
        self.stream.queue_message(msgtype, message)
    }

    pub fn add_msgtype(&mut self, value: i32, sign: &'a [u8], size: usize) -> Result<(), String> {
        if size > DEFAULT_MAX_SIZE_SIZE {
            return Err(format!(
                "size ({}) > MAX_SIZE_SIZE ({})",
                size, DEFAULT_MAX_SIZE_SIZE
            ));
        }
        if sign.len() > DEFAULT_MAX_SIGN_SIZE {
            return Err(format!(
                "size ({}) > MAX_SIGN_SIZE ({})",
                size, DEFAULT_MAX_SIGN_SIZE
            ));
        }
        self.msgtypes.push(MsgType::new(value, sign, size));
        Ok(())
    }

    pub fn add_msgtypes(&mut self, request_types: &'a [RequestType]) -> Result<(), String> {
        for rt in request_types {
            self.add_msgtype(rt.value, rt.sign, rt.size)?;
        }
        Ok(())
    }

    pub fn send_message(&mut self) -> io::Result<usize> {
        self.stream.send_queued_message(&self.config)
    }

    pub fn messages_to_send(&self) -> usize {
        self.stream.to_send()
    }
}
